/*
 * Variable.h
 *
 *  Created on: Mar 27, 2009
 *      Author: Ivan Deras
 */

#ifndef VARIABLE_H_
#define VARIABLE_H_

#include <list>
#include <sstream>
#include "BodyDef.h"
#include "Expression.h"

extern map<string, Type> VariableMap;

using namespace std;

class VariableDef : public BodyDef
{
public:
	VariableDef(string name, int line, int column): BodyDef(name)
	{
		is_array_def = false;
		initial_value = 0;
		this->line = line;
		this->column = column;
	}

	virtual string GenerateBody(){
		ostringstream out;

		VariableMap[name] = variable_type;

		if (!is_array_def){
			cout << name << ": .word 0" << endl;
		} else {
			cout << name << ": .word 0 : " << array_dimension << endl;
		}

		if(initial_value!=0){
			string registerReturn;
			string mipsCode = initial_value->GenerateEvaluation(registerReturn);
			string tempRegister = newTemporaryRegister();

			out << mipsCode << endl
			<<"la " << tempRegister << ", " << name << endl 
			<<"sw " << registerReturn << ", " << "0(" << tempRegister << ")" << endl;

			releaseTemporaryRegister(tempRegister);
			releaseTemporaryRegister(registerReturn);
		}


		return out.str();
	}

	string GenerateLocalBody(int &stackPointerOffset){

	}

	virtual string ToString()
	{
		ostringstream out;

		out << "//Linea: " << line << ": Columna: " << column << endl;
		out << TypeToString(variable_type) << " " << name;



		if (is_array_def)
			out << "[" << array_dimension << "]";

		if (initial_value != 0)
			out << " = " + initial_value->ToString();

		return out.str();
	}

	virtual BodyKind GetKind()
	{
		return bkVariable;
	}

	Type variable_type;
	bool is_array_def;
	Expression *initial_value;
	int array_dimension;
	int line, column;
};

typedef list<VariableDef *> VariableDefList;


#endif /* VARIABLE_H_ */
