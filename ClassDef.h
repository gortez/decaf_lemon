/*
 * Class.h
 *
 *  Created on: Mar 29, 2009
 *      Author: Ivan Deras
 */

#ifndef CLASS_H_
#define CLASS_H_

#include <string>
#include <sstream>
#include "Variable.h"
#include "MethodDef.h"

using namespace std;

class ClassDef
{
	public:
		ClassDef()
		{
			body_def_list = 0;
		}

		ClassDef(string name, BodyDefList *body_def_list)
		{
			// printf("nameee %s\n", name.c_str());
			this->name = name;
			this->body_def_list = body_def_list;
		}

		~ClassDef()
		{
			if (body_def_list != 0) {
				FreeList(body_def_list);

				delete body_def_list;
			}

		}

		string ToString()
		{
			ostringstream out;

			out << "class " << name << "\n";
			out << "{" << "\n";

			if (body_def_list != 0)
				out << ListToString(body_def_list, ";\n", true) << "\n";

			// if (method_def_list != 0)
			// 	out << ListToString(method_def_list, "\n\n", true);

			out << "}" << endl;

			return out.str();
		}

		string GenerateStatement()
		{
			ostringstream out;
			out<<"#class " << name << endl;

			BodyDefList::iterator it= this->body_def_list->begin();

			while (it != this->body_def_list->end()) {
				BodyDef * element = *it;
				if(element->GetKind()==bkVariable){

					out << element->GenerateBody();
				}

				it++;
			}

			out<<"jal main" << endl
			<<"j exit_class"<< endl;

			it= this->body_def_list->begin();

			while (it != this->body_def_list->end()) {
				BodyDef * element = *it;
				if(element->GetKind()==bkMethod){
					
					out << element->GenerateBody();
				}
				
				it++;
			}

			out<<endl<<"exit_class:" << endl
			<< "li $v0, 10"<< endl
			<< "syscall" << endl;

			return out.str();
		}

		void AddFieldDef(VariableDef *field_def);
		void AddMethod(MethodDef *method_def);

		string name;
		BodyDefList *body_def_list;
};

#endif /* CLASS_H_ */
