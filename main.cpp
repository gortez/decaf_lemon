#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <set>
#include <map>
#include <fstream>
#include "ClassDef.h"
#include "tokens.h"

extern int current_line;

static int errors = 0;

using namespace std;

int nextToken(struct TokenData *&info);
 
void* ParseAlloc(void* (*allocProc)(size_t));
void Parse(void* parser, int token, TokenData* tokenInfo);
void ParseFree(void* parser, void(*freeProc)(void*));

// int yyparse();
set<TokenData *> tiSet;


TokenData *allocTokenInfo(int tokenType, string strValue)
{
    TokenData *ti = new TokenData(tokenType, strValue);
    //printf("Alloc: %p\n", ti);
    if (ti != NULL) {
        tiSet.insert(ti);
    }
    
    return ti;
}

ClassDef *class_def;
map<string,string> StringConstants;
// map<char,string> CharConstants;

int main(int argc, char *argv[])
{
	// if (argc > 0) {
	// 	++argv, --argc; /* El primer argumento es el nombre del programa */
	// 	in.open(argv[0], ifstream::in|ifstream::binary);

	// 	if (!in.is_open()) {
	// 		cerr << "No se pudo abrir el archivo " << argv[0] << endl << endl;
	// 		return 0;
	// 	}
	// }
	// else {
	// 	cerr << "Uso: " << argv[0] << " <archivo>" << endl << endl;
	// 	return 0;
	// }

	class_def = 0;
	void *parser = ParseAlloc(malloc);
    TokenData *ti;

    int token = nextToken(ti);

    while (token != KW_EOF) {
        Parse(parser, token, ti);
        // printf("%d", token);
        
        token = nextToken(ti);
    }

    Parse(parser, KW_EOF, ti);
    Parse(parser, 0, NULL);

    ParseFree(parser, free);

	if (class_def != 0) {
		cout << class_def->GenerateStatement() << endl;
	}
	else{
		cout << "empty" << endl;
	}

	return 0;
}

void yyerror(const char *message)
{
	errors++;

	fprintf(stderr, "%d:%s\n", current_line, message);
}
