/*
 * Expression.h
 *
 *  Created on: Mar 27, 2009
 *      Author: Ivan Deras
 */

#ifndef EXPRESSION_H_
#define EXPRESSION_H_

#include <string>
#include <list>
#include <cstring>
#include <cstdlib>
#include "Value.h"
#include "Util.h"

using namespace std;


class Expression
{
	public:
		virtual ExpressionKind GetKind() = 0;
		virtual ResultValue Evaluate() = 0;
		virtual string GenerateEvaluation(string &returnRegister) = 0;
		virtual string ToString() = 0;
		// virtual ~Expression();
};

typedef list<Expression *> ExpressionList;

class BinaryExpression: public Expression
{
	public:
		BinaryExpression(Expression *firstExpression, Expression *secondExpression, ExpressionOperator oper)
		{
			this->firstExpression = firstExpression;
			this->secondExpression = secondExpression;
			this->oper = oper;
		}

		virtual ExpressionKind GetKind()
		{
			return ekBinary;
		}

		virtual ResultValue Evaluate();
		virtual string GenerateEvaluation(string &returnRegister);
		virtual string ToString();

		Expression *firstExpression;
		Expression *secondExpression;
		ExpressionOperator oper;
};

class UnaryExpression: public Expression
{
	public:
		UnaryExpression(Expression *expr, ExpressionOperator oper)
		{
			this->expr = expr;
			this->oper = oper;
		}

		~UnaryExpression()
		{
			if (expr != 0)
				delete expr;
		}

		virtual ExpressionKind GetKind()
		{
			return ekUnary;
		}

		virtual ResultValue Evaluate();
		virtual string GenerateEvaluation(string &returnRegister); 
		virtual string ToString();

		Expression *expr;
		ExpressionOperator oper;
};

class LValueExpression: public Expression
{
	public:
		LValueExpression(string variable_name, Expression *array_index)
		{
			this->variable_name = variable_name;
			this->array_index = array_index;
		}

		~LValueExpression()
		{
			if (array_index != 0)
				delete array_index;
		}

		virtual ExpressionKind GetKind()
		{
			return ekLValue;
		}

		virtual ResultValue Evaluate();
		virtual string GenerateEvaluation(string &returnRegister); 
		virtual string ToString();

		string variable_name;
		Expression *array_index;
};

class MethodCallExpression: public Expression
{
	public:
		MethodCallExpression(string method_name, ExpressionList *method_arguments)
		{
			this->method_name = method_name;
			this->method_arguments = method_arguments;
		}

		~MethodCallExpression()
		{
			if (method_arguments != 0) {
				FreeList(method_arguments);
				delete method_arguments;
			}
		}

		virtual ExpressionKind GetKind()
		{
			return ekMethodCall;
		}

		virtual ResultValue Evaluate();
		virtual string GenerateEvaluation(string &returnRegister); 
		virtual string ToString();

		string method_name;
		ExpressionList *method_arguments;
};

class ConstantExpression: public Expression
{
	public:
		ConstantExpression(int value)
		{
			constant_type = Int;
			constant_value.int_value = value;
		}

		ConstantExpression(bool value)
		{
			constant_type = Boolean;
			constant_value.bool_value = value;
		}

		ConstantExpression(char *value)
		{
			constant_type = String;
			constant_value.string_value = strdup(value);
		}

		~ConstantExpression()
		{
			if (constant_type == String && constant_value.string_value != 0)
				free(constant_value.string_value);
		}

		virtual ExpressionKind GetKind()
		{
			return ekConstant;
		}

		virtual ResultValue Evaluate();
		virtual string GenerateEvaluation(string &returnRegister);
		virtual string ToString();

		Type constant_type;
		union
		{
				int int_value;
				bool bool_value;
				char *string_value;
		} constant_value;
};

#endif /* EXPRESSION_H_ */
