/*
 * Expression.cpp
 *
 *  Created on: Mar 27, 2009
 *      Author: Ivan Deras
 */

#include <sstream>
#include "Expression.h"
#include "MethodDef.h"

extern map<string, MethodDef*> MethodMap;
extern string MethodName;
extern map<string,string> StringConstants;


using namespace std;

string BinaryExpression::GenerateEvaluation(string &returnRegister){
	string leftRegister;
	string rightRegister;
	string firstExprCode = firstExpression->GenerateEvaluation(leftRegister);
	string secondExprCode = secondExpression->GenerateEvaluation(rightRegister);

	releaseTemporaryRegister(leftRegister);
	releaseTemporaryRegister(rightRegister);
	returnRegister = newTemporaryRegister();

	ostringstream out;

	out << firstExprCode
		<< secondExprCode;

	switch(oper){
		case OpAdd:
			out << "add " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpSub:
			out << "sub " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpMul:
			out << "mult " << leftRegister << ", " << rightRegister << endl;
			out << "mflo " << returnRegister <<  endl;
			break;
		case OpDiv:
			out << "div " << leftRegister << ", " << rightRegister << endl;
			out << "mflo " << returnRegister <<  endl;
			break;
		case OpMod:
			out << "div " << leftRegister << ", " << rightRegister << endl;
			out << "mfhi " << returnRegister <<  endl;
			break;
		case OpAnd:
			out << "and " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpOr:
			out << "or " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpGT:
			out << "sgt " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpLT:
			out << "slt " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpGTE:
			out << "sge " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpLTE:
			out << "sle " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpEq:
			out << "seq " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpNotEq:
			out << "sne " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpRShift:
			out << "srlv " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpLShift:
			out << "sllv " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		case OpRot:
			out << "rol " << returnRegister << ", " << leftRegister << ", " << rightRegister << endl;
			break;
		default:
			cout << "Binary operator not supported" << endl;
			exit(0);
	}

	return out.str();
}

string UnaryExpression::GenerateEvaluation(string &returnRegister){
	string expressionRegister;
	string exprMipsCode = expr->GenerateEvaluation(expressionRegister);
	
	releaseTemporaryRegister(expressionRegister);
	returnRegister = newTemporaryRegister();

	ostringstream out;
	out << exprMipsCode ;

	switch(oper){
		case OpAdd:
			out << "addi " << returnRegister << ", " << expressionRegister << ", 1" << endl;
			break;
		case OpSub:
			out << "addi " << returnRegister << ", " << expressionRegister << ", -1" << endl;
			break;
		case OpNot:
			out << "not " << returnRegister << ", " << expressionRegister << endl;
			break;
		default:
			cout << "Unary operator not supported" << endl;
			exit(0);
	}

	return out.str();

}

string LValueExpression::GenerateEvaluation(string &returnRegister){
	stringstream out;
	int offset;
	// Method *m=MethodMap[MethodName];

	// if(m->methodLocalVariables.find(id)!=m->methodLocalVariables.end()){

	// }
	return out.str();
}

string ConstantExpression::GenerateEvaluation(string &returnRegister){
	ostringstream out;

	returnRegister = newTemporaryRegister();

	switch (constant_type)
	{
		case Int: 		out << "li " << returnRegister << ", " << constant_value.int_value << endl; break;
		case Boolean:	out << "li " << returnRegister << ", " << constant_value.bool_value << endl; break;
		case String:	
			string label;

			if(StringConstants.find(constant_value.string_value)==StringConstants.end()){
				label=newStringLabel();
				StringConstants[constant_value.string_value]=label;
			}else{
				label=StringConstants[constant_value.string_value];
			}
			out << "la " << returnRegister << ", " << label << endl;
			break;
	}

	return out.str();
}

string MethodCallExpression::GenerateEvaluation(string &returnRegister)
{
	ostringstream out;
	string parametersConstant[]={"$a0","$a1","$a2","$a3"};

	int index=0;
	
	ExpressionList::iterator it = method_arguments->begin();
	while(it!=method_arguments->end() && index<4)
	{
		Expression *expression = *it;
		string expressionReturnPlace;
		string code = expression->GenerateEvaluation(expressionReturnPlace);
		out << code << endl
			<< "move "<<parametersConstant[index]<<", "<<expressionReturnPlace<<endl;
		releaseTemporaryRegister(expressionReturnPlace);
		it++;
		index++;
	}

	returnRegister = newTemporaryRegister();
	
	out	<< "jal " << this->method_name << endl
		<< "move "<< returnRegister << ", $v0" << endl;


	return out.str();
}

/*
 * Evaluate()
 */
ResultValue BinaryExpression::Evaluate()
{
	ResultValue value;

	/*TODO: Implementar BinaryExpression::Evaluate */

	return value;
}

ResultValue UnaryExpression::Evaluate()
{
	ResultValue value;

	/*TODO: Implementar UnaryExpression::Evaluate */

	return value;
}

ResultValue LValueExpression::Evaluate()
{
	ResultValue value;

	/*TODO: Implementar LValueExpression::Evaluate */

	return value;
}

ResultValue MethodCallExpression::Evaluate()
{
	ResultValue value;

	/*TODO: Implementar MethodCallExpression::Evaluate */

	return value;
}

ResultValue ConstantExpression::Evaluate()
{
	ResultValue value;

	/*TODO: Implementar ConstantExpression::Evaluate */

	return value;
}


/*
 * ToString()
 */

string OperatorToString(ExpressionOperator oper)
{
	switch (oper)
	{
		case OpAdd: return "+";
		case OpSub: return "-";
		case OpMul: return "*";
		case OpDiv: return "/";
		case OpMod: return "%";
		case OpAnd: return "&&";
		case OpOr: return "||";
		case OpNot: return "!";
		case OpGT: return ">";
		case OpLT: return "<";
		case OpGTE: return ">=";
		case OpLTE: return "<=";
		case OpEq: return "==";
		case OpNotEq: return "!=";
		case OpRShift: return ">>";
		case OpLShift: return "<<";
		case OpRot: return "rot";
		default:
				return "Unknown";
	}
}

string BinaryExpression::ToString()
{
	ostringstream out;

	out << firstExpression->ToString() << " " << OperatorToString(oper) << " " << secondExpression->ToString();

	return out.str();
}

string UnaryExpression::ToString()
{
	ostringstream out;

	out << OperatorToString(oper) << expr->ToString();

	return out.str();
}

string LValueExpression::ToString()
{
	ostringstream out;

	out << this->variable_name;

	if (array_index != 0)
		out << "[" << array_index->ToString() << "]";

	return out.str();
}

string MethodCallExpression::ToString()
{
	ostringstream out;

	out << this->method_name << "(" << ListToString(method_arguments, ", ", false) << ")";

	return out.str();
}

string ConstantExpression::ToString()
{
	ostringstream out;

	switch (constant_type)
	{
		case Int: 		out << constant_value.int_value; break;
		case Boolean:	out << constant_value.bool_value; break;
		case String:	out << constant_value.string_value; break;
	}

	return out.str();
}

