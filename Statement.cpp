/*
 * Statement.cpp
 *
 *  Created on: Mar 27, 2009
 *      Author: Ivan Deras
 */

#include "Statement.h"
#include "Util.h"
#include "MethodDef.h"

/*
 * ExecuteStatement


 */
map<string, MethodDef*> MethodDeclaredMap;
string EndMethodLocation;
string CurrentMethodName;

string AssignmentStatement::GenerateStatement(string startLabel, string endLabel)
{
	ostringstream out;

	return out.str();
}

string MethodCallStatement::GenerateStatement(string startLabel, string endLabel)
{
	ostringstream out;
	string parametersConstant[]={"$a0","$a1","$a2","$a3"};

	int index=0;
	
	ExpressionList::iterator it = arguments->begin();
	while(it!=arguments->end() && index<4)
	{
		Expression *expression = *it;
		string expressionReturnPlace;
		string code = expression->GenerateEvaluation(expressionReturnPlace);
		out << code << endl
			<< "move " << parametersConstant[index] << ", " << expressionReturnPlace << endl;
		releaseTemporaryRegister(expressionReturnPlace);
		it++;
		index++;
	}

	out	<< "jal " << this->name << endl;

	return out.str();
}

string IfStatement::GenerateStatement(string startLabel, string endLabel)
{
	ostringstream out;

	string exprPlace;
	string exprCode = condition->GenerateEvaluation(exprPlace);
	releaseTemporaryRegister(exprPlace);

	string elseLabel = newLabel();
	string endIfLabel = newLabel();

	string trueBlockCode = true_part->GenerateStatement(startLabel, endLabel);
	string falseBlockCode;
	if(false_part!=0){
		falseBlockCode = false_part->GenerateStatement(startLabel, endLabel);
	}

	out << exprCode 
		<< "beq " << exprPlace << ", $zero, " << elseLabel << endl
		<< trueBlockCode << endl
		<< "j " << endIfLabel << endl
		<< elseLabel << ":" << endl
		<< falseBlockCode << endl
		<< endIfLabel << ":" << endl;

	return out.str();
}

string WhileStatement::GenerateStatement(string startLabel, string endLabel)
{
	ostringstream out;

	string exprPlace;
	string exprCode = condition->GenerateEvaluation(exprPlace);
	releaseTemporaryRegister(exprPlace);

	startLabel = newLabel();
	endLabel = newLabel();

	string loopBlockCode = loop_body->GenerateStatement(startLabel, endLabel);

	out << startLabel << ":" << endl
		<< exprCode << endl
		<< "beq " << exprPlace << ", $zero, " << endLabel << endl
		<< loopBlockCode << endl
		<< "j " << startLabel << endl
		<< endLabel << ":" << endl;

	return out.str();
}

string ForStatement::GenerateStatement(string startLabel, string endLabel)
{
	ostringstream out;

	string firstAssigmentCode = assignment_list1->GenerateStatement(startLabel, endLabel);

	string exprPlace;
	string exprCode = condition->GenerateEvaluation(exprPlace);
	releaseTemporaryRegister(exprPlace);

	string forStartLabel = newLabel();
	startLabel = newLabel();
	endLabel = newLabel();

	string loopBlockCode = loop_body->GenerateStatement(startLabel, endLabel);
	string secondAssigmentCode = assignment_list2->GenerateStatement(forStartLabel, endLabel);

	out << firstAssigmentCode << endl
		<< forStartLabel << ":" << endl
		<< exprCode << endl
		<< "beq " << exprPlace << ", $zero, " << endLabel << endl
		<< loopBlockCode << endl
		<< startLabel << ":" << endl
		<< secondAssigmentCode << endl
		<< "j " << startLabel << endl
		<< endLabel << ":" << endl;

	return out.str();
}

string ReturnStatement::GenerateStatement(string startLabel, string endLabel)
{
	ostringstream out;
	string exprPlace;
	string exprCode = expr->GenerateEvaluation(exprPlace);

	out << exprCode 
		<< "move $v0, "<< exprPlace << endl
		<< "j " << EndMethodLocation << endl;

	return out.str();
}

string BreakStatement::GenerateStatement(string startLabel, string endLabel)
{
	ostringstream out;

	if(!endLabel.empty())
	{
		out << "j " << endLabel << endl;
	}

	return out.str();
}

string ContinueStatement::GenerateStatement(string startLabel, string endLabel)
{
	ostringstream out;
	
	if(!startLabel.empty())
	{
		out << "j " << startLabel << endl;
	}

	return out.str();
}

string BlockStatement::GenerateStatement(string startLabel, string endLabel)
{
	ostringstream out;

	int offset = 0;
	this->totalOffsetAllocated = variable_def_list->size()*4;
	
	out << "addi $sp, $sp, -"<< this->totalOffsetAllocated <<endl;

	VariableDefList::iterator it = variable_def_list->begin();
	while(it!=variable_def_list->end()){
		VariableDef *variable = *it;

		BlockVariableMetadata varMetadata;
		varMetadata.type = variable->variable_type;
		varMetadata.offset = offset;

		if(variable->initial_value!=0)
		{
			string registerOut;
			string code = variable->initial_value->GenerateEvaluation(registerOut);
			releaseTemporaryRegister(registerOut);
			out << code 
				<<"sw " << registerOut << ", "<< offset << "($sp)" << endl << endl;
		}

		declaredVariables[variable->name] = varMetadata;

		offset+=4;
		it++;
	}

	MethodDef * currentMethod = MethodDeclaredMap[CurrentMethodName];
	currentMethod->blocksInsideMethod.push_back(this);

	StatementList::iterator statementIter = statement_list->begin();
	while(statementIter != statement_list->end()){
		Statement * statement = *statementIter;
		out << statement->GenerateStatement(startLabel, endLabel);

		statementIter++;
	}

	out << "addi $sp, $sp, "<< this->totalOffsetAllocated << endl << endl;

	currentMethod->blocksInsideMethod.pop_back();

	return out.str();
}

/*
 * ToString
 */
string AssignmentStatement::ToString()
{
	ostringstream out;

	out << "// Linea " << line << " Columna " << column << "\n"; 
	out << lvalue->ToString() << " = " << expr->ToString() << ";";

	return out.str();
}

string MethodCallStatement::ToString()
{
	return this->name + "(" + ListToString(arguments, ", ", false) + ");";
}

string IfStatement::ToString()
{
	ostringstream out;

	out << "if (" << condition->ToString() << ")" << endl;
	out << true_part->ToString() << endl;

	if (false_part != 0) {
		out << "else" << endl;
		out << false_part->ToString() << endl;
	}

	return out.str();
}

string WhileStatement::ToString()
{
	ostringstream out;

	out << "while (" << condition->ToString() << ")" << endl;
	out << "{" << loop_body->ToString() << endl << "}";

	return out.str();
}

string ForStatement::ToString()
{
	return "for () {}";
}

string ReturnStatement::ToString()
{
	return "return " + expr->ToString() + ";";
}

string BreakStatement::ToString()
{
	return "break;";
}

string ContinueStatement::ToString()
{
	return "continue;";
}

string BlockStatement::ToString()
{
	ostringstream out;

	out << "{" << endl;

	if (variable_def_list != 0)
		out << ListToString(variable_def_list, ";\n", true) << endl;

	if (statement_list != 0)
		out << ListToString(statement_list, "\n", true);

	out << "}";

	return out.str();
}

