%include
{
#include <iostream>
#include <cassert>
#include <map>
#include <iostream>
#include "Statement.cpp"
#include "MethodDef.cpp"
#include "ClassDef.h"

extern ClassDef *class_def;

VariableDefList *SetType(VariableDefList *list, Type type)
{
	VariableDefList::iterator it = list->begin();

	while (it != list->end()) {
		VariableDef *var = *it;

		var->variable_type = type;
		it++;
	}

	return list;
}


// method_decl_list(mMethodDeclarationList) ::= method_decl(methodDeclaration) method_decl_list(methodDeclarationList) . 
// {
// 	mMethodDeclarationList = methodDeclarationList;
// 	mMethodDeclarationList->push_back(methodDeclaration);
// }
// method_decl_list(mMethodDeclarationList) ::= method_decl(methodDeclaration) .
// { 
// 	mMethodDeclarationList = new MethodDefList;
// 	mMethodDeclarationList->push_back(methodDeclaration);
// }


// return_type(returnType)::= type(TYPE) .{ returnType = TYPE; }
// return_type(returnType)::= K_VOID .{ returnType = Void; }
}

%syntax_error
{
  std::cout << "Error:" << std::endl << " Token Type " << yymajor << std::endl << " number" << TOKEN->number << " text" << TOKEN->text << std::endl;
}

%token_type {TokenData*}
%default_type {int}


%type ID {TokenData*}
%type K_TRUE {ResultValue}
%type K_FALSE {ResultValue}
%type K_INT {Type}
%type K_VOID {Type}
%type K_BOOLEAN {Type}

%type BOOL_OP_OR {TokenData*}
%type BOOL_OP_AND {TokenData*}
%type BIT_SHIFT_OP {TokenData*}
%type REL_OP {TokenData*}
%type ARITH_OP_SUM {TokenData*}
%type ARITH_OP_MUL {TokenData*}

%type CHAR_CONSTANT {TokenData*}
%type INT_CONSTANT {TokenData*}
%type REAL_CONSTANT {TokenData*}
%type STRING_CONSTANT {TokenData*}

%type statement {Statement*}
%type assign {Statement*}
%type method_call {Statement*}
%type if_statement {Statement*}
%type while_statement {Statement*}
%type for_statement {Statement*}
%type return_statement {Statement*}
%type continue_statement {Statement*}
%type block {Statement*}
%type for_assignment_list {Statement*}
%type opt_else {Statement*}
%type break_statement {Statement*}

%type opt_expr {Expression*}
%type expr {Expression*}
%type bool_term {Expression*}
%type relational_expr {Expression*}
%type bit_shift_expr {Expression*}
%type arith_expr {Expression*}
%type arith_term {Expression*}
%type factor {Expression*}
%type constant {Expression*}
%type bool_constant {Expression*}
%type print_argument {Expression*}
%type lvalue {Expression*}
%type opt_array_dimension {Expression*}
%type optional_initialization {Expression*} 

%type opt_method_call_argument_list {ExpressionList*}
%type method_call_argument_list {ExpressionList*}
%type read_argument_list {ExpressionList*}
%type print_argument_list {ExpressionList*}

%type var {VariableDef*}

%type field_decl {VariableDefList*}
%type field_decl_list {VariableDefList*}
%type var_decl {VariableDefList*}
%type var_list {VariableDefList*}
%type opt_var_decl_list {VariableDefList*}
%type var_decl_list {VariableDefList*}

%type optional_body_decl_list {BodyDefList*}
%type body_decl_list {BodyDefList*}
%type body_decl {BodyDefList*}


%type method_decl {MethodDef*}

%type parameter_decl {ParameterDef*}

%type opt_parameter_decl_list {ParameterDefList*}
%type parameter_decl_list {ParameterDefList*}

%type class_definition {ClassDef*}

%type class_name {TokenData*}
%type method_name {TokenData*}

%type opt_statement_list {StatementList*}
%type statement_list {StatementList*}

%type return_type {Type}
%type type {Type}

start::= program.

program ::= class_definition(CLASS_DEF) KW_EOF.	
{
	class_def = CLASS_DEF; 
}

class_definition(CLASS_DEF) ::= K_CLASS class_name(className) KW_KEY_OPEN optional_body_decl_list(mBodyDeclarationList) KW_KEY_CLOSE.
{
	// printf("class_name %s\n", className->text.c_str());
	CLASS_DEF = new ClassDef(className->text, mBodyDeclarationList);
	class_def = CLASS_DEF;
}

optional_body_decl_list(mBodyDeclarationList) ::= . { mBodyDeclarationList = 0; }
optional_body_decl_list(mBodyDeclarationList) ::= body_decl_list(prodBodyDeclaration) .
{ 
	mBodyDeclarationList = prodBodyDeclaration; 
}

body_decl_list(mBodyDeclarationList) ::= body_decl_list(bodyDeclarationList) body_decl(bodyDeclaration). 
{
	mBodyDeclarationList = bodyDeclarationList;
	//mBodyDeclarationList->push_back(bodyDeclaration);

	mBodyDeclarationList->insert(mBodyDeclarationList->end(), bodyDeclaration->begin(), bodyDeclaration->end());
	delete bodyDeclaration;
}
body_decl_list(mBodyDeclarationList) ::= body_decl(bodyDeclaration) .
{ 
	// mBodyDeclarationList = new BodyDefList;
	// mBodyDeclarationList->push_back(bodyDeclaration);
	mBodyDeclarationList = bodyDeclaration; 
}

body_decl(bodyDeclaration)::= method_decl(METHOD_DECL).	
{ 
	bodyDeclaration = new BodyDefList;
	bodyDeclaration->push_back(METHOD_DECL);
	// bodyDeclaration = METHOD_DECL;
}
body_decl(bodyDeclaration)::= field_decl(FIELD_DECL).	
{ 
	bodyDeclaration = new BodyDefList;
	bodyDeclaration->insert(bodyDeclaration->end(), FIELD_DECL->begin(), FIELD_DECL->end());

	// bodyDeclaration = FIELD_DECL; 
}

class_name(ClassName) ::= ID(Identifier). { ClassName = Identifier; }

field_decl(fieldDeclaration) ::= type(TYPE) var_list(VAR_LIST) KW_SEMICOLON.
{
	fieldDeclaration = SetType(VAR_LIST, TYPE);
}

var_list(varList)::= var_list(VAR_LIST) KW_COMMA var(VAR).
{
	varList = VAR_LIST;
	varList->push_back(VAR);
}
var_list(varList)::= var(VAR).
{
	varList = new VariableDefList;
	varList->push_back(VAR);
}

var(Var)::= ID(IDENTIFIER) optional_initialization(OPTIONAL_INITIALIZATION).
{
	Var = new VariableDef(IDENTIFIER->text, IDENTIFIER->lineColumn, 0);
	Var->initial_value = OPTIONAL_INITIALIZATION;
}
var(Var)::= ID(IDENTIFIER) KW_OPEN_BRACKET INT_CONSTANT(Int_Constant) KW_CLOSE_BRACKET.
{
	Var = new VariableDef(IDENTIFIER->text,IDENTIFIER->lineColumn,0);
	Var->is_array_def = true;
	Var->array_dimension = Int_Constant->number;
}

optional_initialization(optionalInitialization)::= KW_EQUAL constant(CONSTANT).
{
	optionalInitialization=CONSTANT;
}
optional_initialization(optionalInitialization)::= .
{
	optionalInitialization=0;
}

method_decl(methodDeclare) ::= type(TYPE) ID(IDENTIFIER) KW_OPEN_PARENTHESIS opt_parameter_decl_list(OPT_PARAMENTER_DECLAR_LIST) KW_CLOSE_PARENTHESIS block(STATEMENT_BLOCK).
{
	methodDeclare = new MethodDef(IDENTIFIER->text); 
	methodDeclare->method_return_type = TYPE;
	methodDeclare->method_parameters = OPT_PARAMENTER_DECLAR_LIST;
	methodDeclare->method_body =  STATEMENT_BLOCK;
}


opt_parameter_decl_list(OptionalParameterDeclarationList)::= parameter_decl_list(PARAMETER_DECL_LIST). 	
{
	OptionalParameterDeclarationList = PARAMETER_DECL_LIST;
}
opt_parameter_decl_list(OptionalParameterDeclarationList)::= .
{
	OptionalParameterDeclarationList = 0; 
}

parameter_decl_list(parameterDeclarationList)::= parameter_decl_list(PARAMETER_DECL_LIST) KW_COMMA parameter_decl(PARAMETER_DECLARATION).
{
	parameterDeclarationList = PARAMETER_DECL_LIST;
	parameterDeclarationList->push_back(PARAMETER_DECLARATION);
}
parameter_decl_list(parameterDeclarationList)::= parameter_decl(PARAMETER_DECLARATION).
{ 
	parameterDeclarationList = new ParameterDefList;
	parameterDeclarationList->push_back(PARAMETER_DECLARATION);
}

parameter_decl(parameterDeclaration)::=type(TYPE) ID(IDENTIFIER).	
{
	parameterDeclaration = new ParameterDef(IDENTIFIER->text, TYPE); 
}

block(block_)::=KW_KEY_OPEN opt_var_decl_list(OPTIONAL_VAR_DECLARATION_LIST) opt_statement_list(OPTIONAL_STATEMENT_LIST) KW_KEY_CLOSE.
{
	block_ = new BlockStatement(OPTIONAL_VAR_DECLARATION_LIST, OPTIONAL_STATEMENT_LIST); 
}

opt_var_decl_list(optionalVarDeclarationList)::= var_decl_list(VAR_DECLARATION_LIST). { optionalVarDeclarationList = VAR_DECLARATION_LIST; }
opt_var_decl_list(optionalVarDeclarationList)::= . { optionalVarDeclarationList = 0; }

opt_statement_list(optionalStatementList)::= statement_list(STATEMENT_LIST). { optionalStatementList = STATEMENT_LIST; }
opt_statement_list(optionalStatementList)::= . { optionalStatementList = 0; }

var_decl_list(varDeclarationList)::= var_decl_list(VAR_DECL_LIST) var_decl(VAR_DECL).
{
	varDeclarationList = VAR_DECL_LIST;
	varDeclarationList->insert(varDeclarationList->end(), VAR_DECL->begin(), VAR_DECL->end());
	delete VAR_DECL;
}
var_decl_list(varDeclarationList)::= var_decl(VAR_DECL).
{ 
	varDeclarationList = VAR_DECL; 
}
var_decl(varDeclaration)::= type(TYPE) var_list(VARIABLE_LIST) KW_SEMICOLON. 
{ 
	varDeclaration = SetType(VARIABLE_LIST, TYPE); 
}

type(type_) ::= K_INT.		{ type_ = Int; }
type(type_) ::= K_BOOLEAN.	{ type_ = Boolean; }
type(type_) ::= K_VOID.		{ type_ = Void; }


statement_list(statementList) ::= statement_list(STATEMENT_LIST) statement(STATEMENT).	
{ 
	statementList = STATEMENT_LIST; statementList->push_back(STATEMENT); 
}
statement_list(statementList) ::= statement(STATEMENT).
{ 
	statementList = new StatementList; statementList->push_back(STATEMENT); 
}

statement(Statement) ::= assign(ASSIGN) KW_SEMICOLON.						{ Statement = ASSIGN; }
statement(Statement) ::= method_call(METHOD_CALL) KW_SEMICOLON.				{ Statement = METHOD_CALL; }
statement(Statement) ::= if_statement(IF_STATEMENT).				{ Statement = IF_STATEMENT; }
statement(Statement) ::= while_statement(WHILE_STATEMENT). 			{ Statement = WHILE_STATEMENT; }
statement(Statement) ::= for_statement(FOR_STATEMENT).				{ Statement = FOR_STATEMENT; }
statement(Statement) ::= return_statement(RETURN_STATEMNT) KW_SEMICOLON.		{ Statement = RETURN_STATEMNT; }
statement(Statement) ::= break_statement(BREAK_STATEMENT) KW_SEMICOLON.		{ Statement = BREAK_STATEMENT; }
statement(Statement) ::= continue_statement(CONTINUE_STATEMENT) KW_SEMICOLON.{ Statement = CONTINUE_STATEMENT; }
statement(Statement) ::= block(BLOCK). 								{ Statement = BLOCK; }

assign(Assign)	::= lvalue(LVALUE) KW_EQUAL expr(EXPR). 
{ 
	Assign = new AssignmentStatement(LVALUE, EXPR,0,0); //@1.first_line, @1.first_column); 
}

method_call(methodCall) ::= method_name(METHOD_NAME) KW_OPEN_PARENTHESIS opt_method_call_argument_list(OPTIONAL_METHOD_CALL_ARG_LIST) KW_CLOSE_PARENTHESIS.	
{ 
	methodCall = new MethodCallStatement(METHOD_NAME->text, OPTIONAL_METHOD_CALL_ARG_LIST, 0, 0); 
}	
method_call(methodCall) ::= K_PRINT print_argument_list(PRINT_ARG_LIST).
{
	methodCall = new MethodCallStatement("print", PRINT_ARG_LIST, 0, 0);//@1.first_line, @1.first_column); 
}
method_call(methodCall) ::= K_READ read_argument_list(READ_ARG_LIST). 
{ 
	methodCall = new MethodCallStatement("read", READ_ARG_LIST, 0, 0);// @1.first_line, @1.first_column); 
}

method_name(methodName) ::= ID(IDENTIFIER).	{ methodName = IDENTIFIER; }

opt_method_call_argument_list(optionalMethodArgList)::= method_call_argument_list(METHOD_CALL_ARG_LIST).	
{ 
	optionalMethodArgList = METHOD_CALL_ARG_LIST; 
}
opt_method_call_argument_list(optionalMethodArgList)::= . 		
{ 
	optionalMethodArgList = 0; 
}

method_call_argument_list(methodCAL)::= method_call_argument_list(ME_CA_AR_LI) KW_COMMA expr(EXPR).
{ 
	methodCAL = ME_CA_AR_LI; methodCAL->push_back(EXPR); 
}
method_call_argument_list(methodCAL)::= expr(EXPR).
{ 
	methodCAL = new ExpressionList; methodCAL->push_back(EXPR); 
}

print_argument_list(printArgumentList)::= print_argument_list(PI_AR_LI) KW_COMMA print_argument(PRINT_ARGUMENT).
{
	printArgumentList = PI_AR_LI; printArgumentList->push_back(PRINT_ARGUMENT); 
}
print_argument_list(printArgumentList)::= print_argument(PRINT_ARGUMENT).			
{ 
	printArgumentList = new ExpressionList; printArgumentList->push_back(PRINT_ARGUMENT); 
}

print_argument(printArgument)::= STRING_CONSTANT(stringConstant).	{ printArgument = new ConstantExpression(stringConstant); }
print_argument(printArgument)::= expr(EXPR).						{ printArgument = EXPR; }

read_argument_list(readArgumentList)::= read_argument_list(RE_AR_LI) KW_COMMA lvalue(LVALUE).
{ 
	readArgumentList = RE_AR_LI; readArgumentList->push_back(LVALUE); 
}
read_argument_list(readArgumentList)::= lvalue(LVALUE).
{ 
	readArgumentList = new ExpressionList; readArgumentList->push_back(LVALUE); 
}

lvalue(LVALUE)	::= ID(IDENTIFIER) opt_array_dimension(OPTIONAL_AR_DI).	{ LVALUE = new LValueExpression(IDENTIFIER->text, OPTIONAL_AR_DI); }

opt_array_dimension(optionalArrayDim) ::= KW_OPEN_BRACKET expr(EXPR) KW_CLOSE_BRACKET.	{ optionalArrayDim = EXPR; }
opt_array_dimension(optionalArrayDim) ::= .	{ optionalArrayDim = 0; }

if_statement(ifStatement)::= K_IF KW_OPEN_PARENTHESIS expr(EXPR) KW_CLOSE_PARENTHESIS block(BLOCK) opt_else(OPT_ELSE).
{ 
	ifStatement = new IfStatement(EXPR, BLOCK, OPT_ELSE, 0, 0);// @1.first_line, @1.first_column); 
}

opt_else(optElse) ::= K_ELSE block(BLOCK).	{ optElse = BLOCK; }
opt_else(optElse) ::= . 				  	{ optElse = 0; }

while_statement(whileStatement)::= K_WHILE KW_OPEN_PARENTHESIS expr(EXPR) KW_CLOSE_PARENTHESIS block(BLOCK).
{ 
	whileStatement = new WhileStatement(EXPR, BLOCK, 0, 0);// @1.first_line, @1.first_column); 
}

for_statement(forStatement)::= K_FOR KW_OPEN_PARENTHESIS for_assignment_list(FOR_ASSIGN_LIST) KW_SEMICOLON expr(EXPR) KW_SEMICOLON for_assignment_list(S_FOR_ASS_LIS) KW_CLOSE_PARENTHESIS block(BLOCK).
{ 
	forStatement = new ForStatement(FOR_ASSIGN_LIST, EXPR, S_FOR_ASS_LIS, BLOCK, 0, 0);//, @1.first_line, @1.first_column); 
}

for_assignment_list(forAssignList)::= for_assignment_list(FOR_ASS_LIS) KW_COMMA assign(ASSIGN).	
{ 
	forAssignList = FOR_ASS_LIS; ((BlockStatement *)forAssignList)->AddStatement(ASSIGN); 
}
for_assignment_list(forAssignList)::= assign(ASSIGN).			
{ 
	forAssignList = new BlockStatement(0, 0);//(@1.first_line, @1.first_column);
	 ((BlockStatement *)forAssignList)->AddStatement(ASSIGN); 
}

return_statement(returnStatement)::= K_RETURN opt_expr(OPTIONAL_EXPR).
{ 
	returnStatement = new ReturnStatement(OPTIONAL_EXPR, 0, 0);// @1.first_line, @1.first_column); 
}

break_statement(breakStatement)::= K_BREAK.
{ 
	breakStatement = new BreakStatement(0, 0);//(@1.first_line, @1.first_column); 
}

continue_statement(continueStatement)::= K_CONTINUE.
{ 
	continueStatement = new ContinueStatement(0, 0);//(@1.first_line, @1.first_column); 
}

opt_expr(optExpr)::= expr(EXPR).	{ optExpr = EXPR;	}
opt_expr(optExpr)::= . 				{ optExpr = 0;		}

expr(Expr)::= expr(EXPR) BOOL_OP_OR(BoolOrOpt) bool_term(BOOL_TERM).{ Expr = new BinaryExpression(EXPR, BOOL_TERM, BoolOrOpt->operatorType); }
expr(Expr)::= bool_term(BOOL_TERM).									{ Expr = BOOL_TERM; }

bool_term(boolTerm)::=bool_term(BOOL_TERM) BOOL_OP_AND(BoolAndOpt) relational_expr(RELATIONAL_EXPR).
{
	boolTerm = new BinaryExpression(BOOL_TERM, RELATIONAL_EXPR, BoolAndOpt->operatorType); 
}
bool_term(boolTerm)::=relational_expr(RELATIONAL_EXPR).
{ 
	boolTerm = RELATIONAL_EXPR; 
}

relational_expr(relationalExpr)::= relational_expr(RELATIONAL_EXPR) REL_OP(RelOp) bit_shift_expr(BIT_SHIFT_EXPR).
{ 
	relationalExpr = new BinaryExpression(RELATIONAL_EXPR, BIT_SHIFT_EXPR, RelOp->operatorType); 
}
relational_expr(relationalExpr)::= bit_shift_expr(BIT_SHIFT_EXPR).
{ 
	relationalExpr = BIT_SHIFT_EXPR; 
}

bit_shift_expr(bitShiftExpr)::=bit_shift_expr(BIT_SHIFT_EXPR) BIT_SHIFT_OP(BitShiftOp) arith_expr(ARITH_EXPR).
{ 
	bitShiftExpr = new BinaryExpression(BIT_SHIFT_EXPR, ARITH_EXPR, BitShiftOp->operatorType); 
}
bit_shift_expr(bitShiftExpr)::=arith_expr(ARITH_EXPR).{ bitShiftExpr = ARITH_EXPR; }

arith_expr(arithExpr)::= arith_expr(ARITH_EXPR) ARITH_OP_SUM(ArithOpSum) arith_term(ARITH_TERM).
{ 
	arithExpr = new BinaryExpression(ARITH_EXPR, ARITH_TERM, ArithOpSum->operatorType); 
}
arith_expr(arithExpr)::= arith_term(ARITH_TERM).	  { arithExpr = ARITH_TERM; }

arith_term(arithTerm)::= arith_term(ARITH_TERM) ARITH_OP_MUL(ArithOpMul) factor(FACTOR).
{ 
	arithTerm = new BinaryExpression(ARITH_TERM, FACTOR, ArithOpMul->operatorType); 
}
arith_term(arithTerm)::= factor(FACTOR).					{ arithTerm = FACTOR; }

factor(Factor)::= KW_EXCLAMATION factor(FACTOR).																	
{ 
	Factor = new UnaryExpression(FACTOR, OpNot); 
}
factor(Factor)::= ARITH_OP_SUM(ArithOpSum) factor(FACTOR).													
{ 
	Factor = new UnaryExpression(FACTOR, ArithOpSum->operatorType); 
}
factor(Factor)::= lvalue(LVALUE).																			
{ 
	Factor = LVALUE; 
}
factor(Factor)::= method_name(METHOD_NAME) KW_OPEN_PARENTHESIS opt_method_call_argument_list(OPT_METHOD_CALL_ARG_LIST) KW_CLOSE_PARENTHESIS.
{ 
	Factor = new MethodCallExpression(METHOD_NAME->text, OPT_METHOD_CALL_ARG_LIST); 
}
factor(Factor)::= constant(CONSTANT).		
{ 
	Factor = CONSTANT; 
}
factor(Factor)::= KW_OPEN_PARENTHESIS expr(EXPR) KW_CLOSE_PARENTHESIS.																				
{ 
	Factor = EXPR;
}

constant(Constant)::= INT_CONSTANT(IntConstant).   { Constant = new ConstantExpression(IntConstant->number); }
constant(Constant)::= CHAR_CONSTANT(CharConstant). { Constant = new ConstantExpression(CharConstant->character); }
constant(Constant)::= REAL_CONSTANT(RealConstant). { Constant = new ConstantExpression(RealConstant->number); }
constant(Constant)::= bool_constant(BoolConstant). { Constant = BoolConstant; }

bool_constant(boolConstant)::= K_TRUE.	{ boolConstant = new ConstantExpression(true); }
bool_constant(boolConstant)::= K_FALSE.	{ boolConstant = new ConstantExpression(false); }







