/*
 * Value.h
 *
 *  Created on: Mar 27, 2009
 *      Author: ivan_deras
 */

#ifndef VALUE_H_
#define VALUE_H_
#include <cstdio>
#include <sstream>
#include <iostream>
#include <string>
#include <list>
#include <map>
#include <set>

using namespace std;

typedef unsigned char byte;

/*
 * Tipos de datos soportados por Decaf
 */
enum Type {
	Int,
	Char,
	Boolean,
	String,
	Void
};

static string CurrentMethodEvaluated;

static map<string, Type> VariableMap;



static const char *TemporaryRegisters[] = {"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8", "$t9"};
static const char *SaveRegisters[] = {"$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7"};
static set<string> UsedTemporaryRegisters;
static set<string> UsedSaveRegisters;

inline string newTemporaryRegister() {
	for (int i = 0; i < 10; ++i)
	{
		if(UsedTemporaryRegisters.find(TemporaryRegisters[i])==UsedTemporaryRegisters.end()){
			UsedTemporaryRegisters.insert(TemporaryRegisters[i]);
			return string(TemporaryRegisters[i]);
		}
	}

	cout << "Error" << endl << "No more temporary register available";
	exit(0);

	return string("");
}

inline string newSaveRegister() {
	for (int i = 0; i < 10; ++i)
	{
		if(UsedSaveRegisters.find(SaveRegisters[i])==UsedSaveRegisters.end()){
			UsedSaveRegisters.insert(SaveRegisters[i]);
			return string(SaveRegisters[i]);
		}
	}

	cout << "Error" << endl << "No more Save register available";
	exit(0);

	return string("");
}

inline void releaseTemporaryRegister(string temporaryRegister){
	UsedTemporaryRegisters.erase(temporaryRegister);
}

inline void releaseSaveRegister(string saveRegister){
	UsedSaveRegisters.erase(saveRegister);
}

static int labelCount;

inline string newLabel() {
	ostringstream out;
	out << "label" << labelCount++;
	return out.str();
}

inline string newStringLabel() {
	ostringstream out;
	out << "stringlabel" << labelCount++;
	return out.str();
}

inline string newCharLabel() {
	ostringstream out;
	out << "charlabel" << labelCount++;
	return out.str();
}



inline string TypeToString(Type type)
{
	switch (type)
	{
		case Int: return "int";
		case Char: return "char";
		case Boolean: return "boolean";
		case String: return "string";
		case Void: return "void";
	}

	return "None";
}

/*
 * Esta estructura representa el resultado de la evaluación de una Expresion.
 * El caso de string_value será utilizado para la proposicion print, la cual
 * permite un string como parametro,
 */
struct ResultValue
{
	Type type;
	union {
		int int_value;
		bool bool_value;
		char *string_value;
	} value;
};

/*
 * Esta estructura representa una referencia a una variable.
 */
struct LValue
{
	Type type;
	byte *ptr;
};


enum ExpressionKind
{
	ekBinary,
	ekUnary,
	ekLValue,
	ekMethodCall,
	ekConstant
};

/*
 * Operadores validos en una expression (binaria o unaria)
 *  1. OpAdd = Suma
 *  2. OpSub = Resta
 *  3. OpMul = Multiplicacion
 *  4. OpDiv = Division
 *  5. OpMod = Modulo
 *  6. OpAnd = Y booleano
 *  7. OpOr = O booleano
 *  8. OpNot = Negacion booleana
 *  9. OpGT = Mayor que
 * 10. OpLT = Menor que
 * 11. OpGTE = Mayor o Igual que
 * 12. OpLTE = Menor o Igual que
 * 13. OpEq = Igualdad
 * 14. OpNotEq = No igual
 * 15. OpRShift = Corrimiento de bits a la derecha
 * 16. OpLShift = Corrimiento de bits a la izquierda
 * 17. OpRot = Rotacion de bits
 */

enum ExpressionOperator
{
	OpAdd,
	OpSub,
	OpMul,
	OpDiv,
	OpMod,
	OpAnd,
	OpOr,
	OpNot,
	OpGT,
	OpLT,
	OpGTE,
	OpLTE,
	OpEq,
	OpNotEq,
	OpRShift,
	OpLShift,
	OpRot
};

struct TokenData {
	int number;
	char character;
	double floating;
	string text;
	bool boolean;
	int lineColumn;
	ExpressionOperator operatorType;

	TokenData(int tokenType, string strValue) {
	    this->number = tokenType;
	    this->text = strValue;
	}
};

#endif /* VALUE_H_ */
