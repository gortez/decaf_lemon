#ifndef BODYDEF_H_
#define BODYDEF_H_

#include <list>
#include <sstream>
using namespace std;

enum BodyKind
{
	bkMethod,
	bkVariable
};

class BodyDef
{
	public:
		BodyDef(string name)
		{
			this->name = name;
		}

		virtual BodyKind GetKind() = 0;
		virtual string GenerateBody() = 0;
		virtual string ToString() = 0;

		string name;
};

typedef list<BodyDef *> BodyDefList;

#endif /* BODYDEF_H_ */