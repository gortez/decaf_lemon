DECAF_LEXER 	:= decaf_lexer.cpp
DECAF_GRAMMAR	:= decaf_grammar.cpp
DECAF_TOKENS	:= decaf_tokens.h
DECAF_LEMON		:= decaf_lemon.cpp
TARGET  := decaf
SRCS    := ${DECAF_LEMON} ${DECAF_LEXER}  Expression.cpp Util.cpp main.cpp 
OBJS    := ${SRCS:.cpp=.o} 

CCFLAGS = -g
LDFLAGS = 
LIBS    = 

.PHONY: all clean

all:: ${TARGET} 

${TARGET}: ${OBJS} 
	${CXX} ${LDFLAGS} -o $@ $^ ${LIBS} 

${OBJS}: %.o: %.cpp
	${CXX} ${CCFLAGS} -o $@ -c $<
	
${DECAF_LEXER}:	decaf_lexer.l
	flex -o $@ $<
	
${DECAF_LEMON}: decaf_lemon.y
	./lemon -Tlempar.c $<
	mv decaf_lemon.c $@
	mv decaf_lemon.h tokens.h

${DECAF_GRAMMAR} ${DECAF_TOKENS}: decaf_grammar.y
	bison --defines=${DECAF_TOKENS} -r all -o ${DECAF_GRAMMAR} $<

clean:: 
	-rm -f *~ *.o ${TARGET} ${DECAF_LEXER} ${DECAF_GRAMMAR} ${DECAF_TOKENS} $(DECAF_LEMON) decaf_grammar.output

