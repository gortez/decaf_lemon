#include "MethodDef.h"


string MethodDef::GenerateBody(){
	ostringstream out;

	string parameters[]={"$a0","$a1","$a2","$a3"};

	string methodNameBackup=CurrentMethodName;
	string previousEndBackup=EndMethodLocation;

	CurrentMethodName = this->name;
	EndMethodLocation = "End"+this->name;

	int paramsOffset = 4;
	if(this->method_parameters!=0){
		paramsOffset = (method_parameters->size()*4)+4;	
	}

	int index = 0;
	MethodDeclaredMap[this->name] = this;

	out	<< this->name<<":"<< endl
		<< "sw $ra, 0($sp)" << endl
		<< "addi $sp, $sp, -" << paramsOffset <<endl;

	int offsetPosition = 4; // due to $ra beign at offset 0

	BlockStatement * paramBlock = new BlockStatement(0,0);
	paramBlock->totalOffsetAllocated = paramsOffset;

	if(this->method_parameters!=0){
		ParameterDefList::iterator paramIter = this->method_parameters->begin();
		while(paramIter!=this->method_parameters->end()){
			ParameterDef *parameter =* paramIter;

			BlockVariableMetadata varMetadata;
			varMetadata.type = parameter->parameter_type;
			varMetadata.offset = offsetPosition;
			paramBlock->declaredVariables[parameter->parameter_name] = varMetadata;

			out << "sw " << parameters[index++] << ", " << offsetPosition << "($sp)" <<endl;
			paramIter++;
			offsetPosition+=4;
		}
	}
	

	this->blocksInsideMethod.push_back(paramBlock);

	string blockCode = this->method_body->GenerateStatement("", "");

	int remainingOffset = this->getTotalOffsetRemaining();
	out << blockCode << endl
		<< EndMethodLocation << ":" << endl
		<< "addi $sp, $sp, " << remainingOffset << endl
		<< "lw $ra, 0($sp)" << endl
		<< "jr $ra" << endl;

	CurrentMethodName=methodNameBackup;
	EndMethodLocation=previousEndBackup;

	return out.str();
}