/*
 * MethodDef.h
 *
 *  Created on: Mar 28, 2009
 *      Author: Ivan Deras
 */

#ifndef METHODDEF_H_
#define METHODDEF_H_

#include <list>
#include <sstream>
#include "BodyDef.h"
#include "Statement.h"

using namespace std;

struct ParameterDef
{
	ParameterDef(string name, Type type)
	{
		parameter_name = name;
		parameter_type = type;
	}

	string ToString()
	{
		return TypeToString(parameter_type) + " " + parameter_name;
	}

	string parameter_name;
	Type parameter_type;
};

typedef list<ParameterDef *> ParameterDefList;


class MethodDef : public BodyDef
{
	public:
		MethodDef(string name): BodyDef(name){}

		~MethodDef()
		{
			if (method_parameters != 0) {
				ParameterDefList::iterator it = method_parameters->begin();

				while (it != method_parameters->end()) {
					ParameterDef *p = *it;
					delete p;

					it++;
				}

				method_parameters->clear();
				delete method_parameters;
			}

			if (method_body != 0)
				delete method_body;
		}

		virtual string GenerateBody();

		virtual string ToString()
		{
			ostringstream out;

			out << TypeToString(method_return_type) << " " << name << "(";
			if (method_parameters != 0)
				out << ListToString(method_parameters, ", ", false);
			out << ")" << endl;
			out << method_body->ToString() << endl;

			return out.str();
		}

		virtual BodyKind GetKind()
		{
			return bkMethod;
		}

		Type method_return_type;
		ParameterDefList *method_parameters;
		Statement *method_body;
		list<BlockStatement*> blocksInsideMethod;

		int getOffsetFromVariable(string variableName){
			int offset = 0;

			list<BlockStatement*>::iterator it = blocksInsideMethod.end();
			while(it!=blocksInsideMethod.begin())
			{
				it--;
				BlockStatement *block = *it;
				map<string, BlockVariableMetadata>::iterator localVarIterator;
				localVarIterator = block->declaredVariables.find(variableName);
				if(localVarIterator != block->declaredVariables.end()){

					return offset + localVarIterator->second.offset;
				}

				offset += block->totalOffsetAllocated;
			}

			cout << "Variable with name: " << variableName << " not found" << endl;
			exit(0);
			return -1;
			
		}


	private:
		int getTotalOffsetRemaining(){
			int offset = 0;

			list<BlockStatement*>::iterator it = blocksInsideMethod.begin();
			while(it!=blocksInsideMethod.end())
			{
				BlockStatement *block = *it;
				
				offset += block->totalOffsetAllocated;
				it++;
			}
			return offset;
		}
};

typedef list<MethodDef *> MethodDefList;

extern map<string, MethodDef*> MethodDeclaredMap;
extern string CurrentMethodName;
extern string EndMethodLocation;



#endif /* METHODDEF_H_ */
