%option noyywrap

%{

#include <math.h>
#include "Value.h"
#include "tokens.h"

// #define YY_INPUT(buf,result,max_size)  {\
//     result = GetNextChar(buf, max_size); \
//     if (  result <= 0  ) \
//       result = YY_NULL; \
//     }
TokenData *allocTokenInfo(int tokenType, string strValue);

int line = 1;
#define YY_DECL int nextToken(struct TokenData *&info)

#define RETURN_TOKEN(tk) do { \
	info = NULL; \
	return tk; \
} while(0)

%}

DIGIT    [0-9]
LETTER   [a-zA-Z]

%%

"class"		{ RETURN_TOKEN(K_CLASS); }
"break"		{ RETURN_TOKEN(K_BREAK); }
"continue"	{ RETURN_TOKEN(K_CONTINUE); }
"while"		{ RETURN_TOKEN(K_WHILE); }
"for"		{ RETURN_TOKEN(K_FOR); }
"if"		{ RETURN_TOKEN(K_IF); }
"else"		{ RETURN_TOKEN(K_ELSE); }
"return"	{ RETURN_TOKEN(K_RETURN); }
"read"		{ RETURN_TOKEN(K_READ); }
"print"		{ RETURN_TOKEN(K_PRINT); }
"true"		{ RETURN_TOKEN(K_TRUE); }
"false"		{ RETURN_TOKEN(K_FALSE); }
"int"		{ RETURN_TOKEN(K_INT); }
"void"		{ RETURN_TOKEN(K_VOID); }
"boolean"	{ RETURN_TOKEN(K_BOOLEAN); }
"||"		{ info = allocTokenInfo(BOOL_OP_OR, yytext); info->operatorType = OpOr; return BOOL_OP_OR; }
"&&"		{ info = allocTokenInfo(BOOL_OP_AND, yytext); info->operatorType = OpAnd; return BOOL_OP_AND; }
">>"		{ info = allocTokenInfo(BIT_SHIFT_OP, yytext); info->operatorType = OpRShift; return BIT_SHIFT_OP; }
"<<"		{ info = allocTokenInfo(BIT_SHIFT_OP, yytext); info->operatorType = OpLShift; return BIT_SHIFT_OP; }
"rot"		{ info = allocTokenInfo(BIT_SHIFT_OP, yytext); info->operatorType = OpRot; return BIT_SHIFT_OP; }
">"			{ info = allocTokenInfo(REL_OP, yytext); info->operatorType = OpGT; return REL_OP; }
"<"			{ info = allocTokenInfo(REL_OP, yytext); info->operatorType = OpLT; return REL_OP; }
">="		{ info = allocTokenInfo(REL_OP, yytext); info->operatorType = OpGTE; return REL_OP; }
"<="		{ info = allocTokenInfo(REL_OP, yytext); info->operatorType = OpLTE; return REL_OP; }
"=="		{ info = allocTokenInfo(REL_OP, yytext); info->operatorType = OpEq; return REL_OP; }
"!="		{ info = allocTokenInfo(REL_OP, yytext); info->operatorType = OpNotEq; return REL_OP; }
"+"			{ info = allocTokenInfo(ARITH_OP_SUM, yytext); info->operatorType = OpAdd; return ARITH_OP_SUM; }
"-"			{ info = allocTokenInfo(ARITH_OP_SUM, yytext); info->operatorType = OpSub; return ARITH_OP_SUM; }
"*"			{ info = allocTokenInfo(ARITH_OP_MUL, yytext); info->operatorType = OpMul; return ARITH_OP_MUL; }
"/"			{ info = allocTokenInfo(ARITH_OP_MUL, yytext); info->operatorType = OpDiv; return ARITH_OP_MUL; }
"%"			{ info = allocTokenInfo(ARITH_OP_MUL, yytext); info->operatorType = OpMod; return ARITH_OP_MUL; }
\"([^"]|\"\")*\"	{
						info = allocTokenInfo(STRING_CONSTANT, yytext);
						//info->text = strdup(yytext);
						return STRING_CONSTANT;
					}
{DIGIT}+		{
					info = allocTokenInfo(INT_CONSTANT, yytext);
					info->number = atoi(yytext);
					return INT_CONSTANT;
				}
(_|{LETTER})(_|{LETTER}|{DIGIT})*	{
										info = allocTokenInfo(ID, yytext);
										return ID;
									}
";"		{ RETURN_TOKEN(KW_SEMICOLON); }
","		{ RETURN_TOKEN(KW_COMMA); }
"("		{ RETURN_TOKEN(KW_OPEN_PARENTHESIS); }
")"		{ RETURN_TOKEN(KW_CLOSE_PARENTHESIS); }
"{"		{ RETURN_TOKEN(KW_KEY_OPEN); }
"}"		{ RETURN_TOKEN(KW_KEY_CLOSE); }
"["		{ RETURN_TOKEN(KW_OPEN_BRACKET); }
"]"		{ RETURN_TOKEN(KW_CLOSE_BRACKET); }
"="		{ RETURN_TOKEN(KW_EQUAL); }
"!"		{ RETURN_TOKEN(KW_EXCLAMATION); }
"//".*"\n"	{ }
[ \t]+		{ }
"\n"		{ line++; }
<<EOF>> { return KW_EOF;}
.           printf( "%d:Caracter no reconocido: #%d, %s\n", line, yytext[0], yytext );

%%


